#!/bin/bash
source /etc/profile

if [ "$1" == "" ] || [ "$2" == "" ]; then
    echo "bash search.sh <imagePath> <outputResultPath>"
else
   alg="CEDD"
	if [ "$3" != "" ]; then
		alg="$3"
	fi

	java -cp /home/student/Dragonfly/bin/Dragonfly.jar dragonfly.searcher.Searcher \
	-inputImage $1 \
	-outputResult $2 \
	-localIndexDir /home/student/Dragonfly/MyIndex \
	-alg $alg
fi


