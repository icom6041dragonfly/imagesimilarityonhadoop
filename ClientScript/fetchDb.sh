#!/bin/bash

source /etc/profile
echo "Remove DB at Local"
rm -r ~/Dragonfly/MyIndex
echo "Fetch latest Lucene DB from HDFS to Local"
hdfs dfs -copyToLocal -ignoreCrc /dragonfly/output/MyIndex_c ~/Dragonfly/MyIndex
echo "End."

