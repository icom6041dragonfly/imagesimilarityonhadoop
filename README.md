# Dragonfly

Drongonfly is an MapReduce application to perform distributed image fetching with HIPI and distributed feature extraction with LIRE, and image searching with LIRE library.

* [Wiki/README]: https://bitbucket.org/icom6041dragonfly/imagesimilarityonhadoop/wiki
* Get the source code on [Dragonfly Bitbucket Git]: https://bitbucket.org/icom6041dragonfly/imagesimilarityonhadoop

### Data Input Format
Dragonfly supports images input from Nutch on Solr and CSV file (separated by "|"). For Solr based data source, please make sure the image url is located in either [id] or [url] field, and source site field at either [site] or [domain] field of the documents. For CSV file based input, with separate "|", the 1st column is image url and the 2nd column is source site url.

### Nutch & Solr setup
##### Start Solr web service
After basic Nutch and Solr installation on official guideline, Start the Solr webservice by:
```
#!sh
/opt/solr/solr-5.2.1/bin/solr start
```

##### Create Solr Core for Nutch
Create a core on Solr for a Nutch crawling with syntax below:

```
#!sh

/opt/solr/solr-5.2.1/bin/solr create_core -c <core_name> -d <confdir>
```

###### Example:
```
#!sh
/opt/solr/solr-5.2.1/bin/solr create_core -c ICOM6041 -d basic_configs
```

##### Perform Nutch all-in-one crawling
This command & param syntax can perform crawling:

```
#!sh

/opt/nutch/apache-nutch-1.10/bin/crawl -i -D solr.server.url=http://localhost:8983/solr/<core_name> urls_<core_name>/ crawl_<core_name>/ <num_of_round>
```


###### Example:
```
#!sh

/opt/nutch/apache-nutch-1.10/bin/crawl -i -D solr.server.url=http://localhost:8983/solr/ICOM6041 urls_ICOM6041/ crawl_ICOM6041/ 5
```



### Dragonfly Installation steps
1. Download and build the project. The distributables should be exported to directory [dist]
2. Upload the dragonfly distributables to desired location to a node on the cluster. In the demonstration, we put it to ~/Dragonfly/bin/ on Master Node student68-x1.
3. If the Image searching client machine is not on the Master Node, upload the dragonfly distributables to the client machine as well. In the demonstration, we put it to ~/Dragonfly/bin/ on Salve Node student66-x1 with our PHP website ready.

### Execution on Distrubted Image Feature Re
##### Parameters
You can apply Dragonfly-Downloader-specific configurations as below:

* downloader.outfile: HDFS location to store the output HIBs
* downloader.inputPath: Define the data input file source on HDFS to be **read**. With SolrUrl defined (i.e. Solr mode), this is the location to **create & read** the data input file basing on Solr Data Source instead.
* *(Optional)* dragonfly.preserveAsDistributed: Indicate whether preserve the sub-HIB after Master HIB is merged on Reducer. (Recommended: true)
* *(Optional)* dragonfly.cleanStart: Indicate whether delete the output directory at the beginning.  (Recommended: true)
* *(Optional)* downloader.nodeNum: Number of unique Node preferred to be used on the Cluster. For our case, we use 5 slave nodes.

And some Dragonfly-specific configuration for Solr mode

* *(Optional)* downloader.solrUrl: Solr JSON data retrival link
* *(Optional)* downloader.solrStartAt: Define preferred starting document position
* *(Optional)* downloader.solrMaxCount: Limit the Solr document reading count

(the standard MapReduce run-time configuration is also supported) 

###### Example
Since Downloader depends on some 3rd party libraries, -libjars is required to submit the job with these libraries to the cluster
```
#!sh
export MyOtherJar=/home/hduser/Dragonfly/bin/lib/hipi-2.0.jar,/home/hduser/Dragonfly/bin/lib/commons-imaging-1.0-SNAPSHOT.jar,/home/hduser/Dragonfly/bin/lib/jackson-core-2.6.1-SNAPSHOT.jar
```
As the execution time can be very long, timeout is disabled by *-Dmapreduce.task.timeout=0*
```
#!sh
yarn jar ~/Dragonfly/bin/Dragonfly.jar \
dragonfly.downloader.Downloader \
-libjars ${MyOtherJar} \
-Dmapreduce.job.name=DownloadNPreserve_Demo \
-Ddragonfly.preserveAsDistributed=true \
-Ddragonfly.cleanStart=true \
-Ddownloader.solrStartAt=0 \
-Dmapreduce.task.timeout=0 \
-Ddownloader.solrMaxCount=35000 \
-Ddownloader.solrUrl="http://202.45.128.135:10070/solr/demo/select?q=id%3A*.jpg&fl=domain%2C+id&wt=json&indent=true" \
-Ddownloader.inputPath=/dragonfly/input/imagesFromSolrDemo.txt \
-Ddownloader.outfile=/dragonfly/hib/SolrDemo \
-Ddownloader.nodeNum=5 
```

### Execution on Distrubted Image Feature Retrival
##### Parameters
You can apply Dragonfly-Indexer-specific configurations as below:

* indexer.inputPath: HDFS HIBs input directory
* indexer.outputPath: HDFS Index output directory
* *(Optional)* indexer.nodes: Define the data input file source on HDFS to be **read**. With SolrUrl defined (i.e. Solr mode), this is the location to **create & read** the data input file basing on Solr Data Source instead.
* *(Optional)* indexer.localTempPath: Define local temp storage location for subIndex

(the standard MapReduce run-time configuration is also supported) 

###### Example
Since Indexer depends on some 3rd party libraries, -libjars is required to submit the job with these libraries to the cluster
```
#!sh
export MyOtherJar=/home/hduser/Dragonfly/bin/lib/lire.jar,/home/hduser/Dragonfly/bin/lib/hipi-2.0.jar,/home/hduser/Dragonfly/bin/lib/commons-math3-3.2.jar,/home/hduser/Dragonfly/bin/lib/JOpenSurf.jar,/home/hduser/Dragonfly/bin/lib/lucene-analyzers-common-4.10.2.jar,/home/hduser/Dragonfly/bin/lib/lucene-core-4.10.2.jar,/home/hduser/Dragonfly/bin/lib/lucene-queryparser-4.10.2.jar,/home/hduser/Dragonfly/bin/lib/commons-imaging-1.0-SNAPSHOT.jar
```

As the execution time can be very long, timeout is disabled by *-Dmapreduce.task.timeout=0*
```
#!sh
yarn jar ~/Dragonfly/bin/Dragonfly.jar \
dragonfly.indexer.Indexer \
-libjars ${MyOtherJar} \
-Dmapreduce.task.timeout=0 \
-Dmapreduce.job.name=Indexer_Demo \
-Dindexer.inputPath=/dragonfly/hib/SolrDemo_dist \
-Dindexer.outputPath=/dragonfly/output/SolrDemoIndex \
-Dindexer.nodes=5 \
-Dindexer.localTempPath=~/tmp/Dragonfly/finalIndex
```

### Perform Searching on Client Machine

##### Fetching Image Feature Index to Client Machine
Execute this command to fetch the lastest Lucene Index from HDFS to Local
```
#!sh
bash ~/Dragonfly/fetchDb.sh
```

##### Search
You can apply Dragonfly-Searcher-specific configurations as below:

* inputImage: Path of input image
* outputResult: Path of output result
* localIndexDir: Path of Image Feature Index Lucene Directory
* alg: Feature Descriptor (Default = CEDD). Supporting:
    * CEDD: Color Edge and Directivity Descriptor (Color-pattern balanced)
    * FC: FCTH, fcth fuzzy color and texture histogram  (Color-pattern balanced)
    * PH: PHOG, Pyramid Histogram of Oriented Gradients (Pattern-first)
    * CL: Color Layout (Color-first)

###### Example
```
#!sh
java -cp /home/student/Dragonfly/bin/Dragonfly.jar dragonfly.searcher.Searcher \
	-inputImage ~/Dragonfly/input/blockD.jpg \
	-outputResult ~/Dragonfly/result.txt \
	-localIndexDir /home/student/Dragonfly/MyIndex \
	-alg FC
```
##### Alternative way to search
This script wrapped the java command above, providing easier interface for other platform (eg PHP) to call:
```
#!sh
bash ~/Dragonfly/search.sh <imagePath> <outputResultPath> <(Optional) Feature Descriptor>
```
###### Example
```
#!sh
bash ~/Dragonfly/search.sh ~/Dragonfly/input/blockD.jpg ~/Dragonfly/result.txt FC
```

### Output Sample (Same on Screen [System.out] and output file)
```
http://static-origin.zalora.com.hk/p/Faux-Leather-Lace-Applique-580091-1.jpg|http://zalora.com.hk
http://static-origin.zalora.com.hk/p/Formal-Long-Sleeves-Beaded-Jersey-Short-Dress-718951-1.jpg|http://zalora.com.hk
http://static-origin.zalora.com.hk/p/Puff-Sleeve-Bodycon-963861-1.jpg|http://zalora.com.hk
```