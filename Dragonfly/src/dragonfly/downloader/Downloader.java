/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dragonfly.downloader;

import hipi.image.FloatImage;
import hipi.imagebundle.HipiImageBundle;
import hipi.image.ImageHeader;
import hipi.image.ImageHeader.ImageType;
import hipi.image.io.JPEGImageUtil;
import hipi.image.io.PNGImageUtil;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.BufferedReader;
import java.io.StringReader;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.net.URLConnection;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.ExifTagConstants;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputDirectory;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;
import org.apache.hadoop.fs.FileStatus;

import java.io.IOException;
import java.net.URL;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A utility MapReduce program that takes a list of image URL's, downloads them,
 * and creates a {@link hipi.imagebundle.HipiImageBundle} from them.
 *
 * When running this program, the user must specify 3 parameters. The first is
 * the location of the list of URL's (one URL per line), the second is the
 * output path for the HIB that will be generated, and the third is the number
 * of nodes that should be used during the program's execution. This final
 * parameter should be chosen with respect to the total bandwidth your
 * particular cluster is able to handle. An example usage would be: <br />
 * <br />
 * downloader.jar /path/to/urls.txt /path/to/output.hib 10 <br />
 * <br />
 * This program will automatically force 10 nodes to download the set of URL's
 * contained in the input list, thus if your list contains 100,000 images, each
 * node in this example will be responsible for downloading 10,000 images.
 *
 */
public class Downloader extends Configured implements Tool {

    final static SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static class DownloaderMapper extends Mapper<IntWritable, Text, BooleanWritable, Text> {

        private static Configuration conf;
        private FileSystem hdfs;
        private boolean isCompleted = false;
        private List<String> createdHibPaths = new ArrayList<>();
        HipiImageBundle hib = null;

        @Override
        public void setup(Context context) throws IOException, InterruptedException {
            this.conf = context.getConfiguration();
            hdfs = FileSystem.get(conf);
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            System.out.println("Clean-up Mapper " + context.getTaskAttemptID().toString());  //debug
            System.out.println("Mapper Status = " + context.getStatus());  //debug
            System.out.println("Mapper Progress = " + context.getProgress());  //debug
            System.out.println("Mapper need Cleanup = " + context.getTaskCleanupNeeded());  //debug

            if (!isCompleted) {
                System.out.println("This is an incomplete Map attempt " + context.getTaskAttemptID().toString() + ". Cleanup to delete unuseful hib");
                
                if (hib != null){
                    try {
                        // Cleanup
                        hib.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            
                // Skip this part to keep partial HIBs even for incomplete task
//                for (String hibPath : createdHibPaths) {
//                    try {
//                        hdfs.delete(new Path(hibPath), false);
//                        hdfs.delete(new Path(hibPath + ".dat"), false);
//                    } catch (Exception e) {
//                        System.err.println("Cleanup delete of killed/failed attempt cannot delete " + hibPath);
//                        continue;
//                    }
//                }
            }
            super.cleanup(context); //To change body of generated methods, choose Tools | Templates.
            hdfs.close();
            
        }

        // Download images at the list of input URLs and store them in a temporary HIB.
        @Override
        public void map(IntWritable key, Text value, Context context) throws IOException, InterruptedException {

            // Create path for temporary HIB file
//            String tempPath = conf.get("downloader.outpath") + key.get() + ".hib.tmp";
            //Boolean preserveAsDistributed = conf.getBoolean("dragonfly.preserveAsDistributed", false);
            String distDestDir = "";
            //if (preserveAsDistributed) {
            distDestDir = conf.get("downloader.outfile") + "_dist/";
            hdfs.mkdirs(new Path(distDestDir));
            //}
            int attemptId = context.getTaskAttemptID().getId();
            String tempPath = distDestDir + key.get() + "_" + attemptId + ".dist.hib";
            //String tempPath = conf.get("downloader.outpath") + key.get() + ".dist.hib";
            createdHibPaths.add(tempPath);
            //HipiImageBundle hib = new HipiImageBundle(new Path(tempPath), conf);
            hib = new HipiImageBundle(new Path(tempPath), conf);
            hib.open(HipiImageBundle.FILE_MODE_WRITE, true);

//            FileSystem localFs = FileSystem.getLocal(conf);
//            String localTempDirStr = "~/downloader_tmp";
//            String localTempFilenameStr = "tempImg";
//            removeDir(localTempDirStr, localFs);
//            localFs.mkdirs(new Path(localTempDirStr));
            // The value argument contains a list of image URLs delimited by \n. Setup buffered reader to allow processing this string line by line.
            BufferedReader reader = new BufferedReader(new StringReader(value.toString()));
            String line, uri, site;
            int i = key.get();
            int iprev = i;

            // Iterate through URLs
            while ((line = reader.readLine()) != null) {
                String[] splits = line.split("\\|");
                uri = splits[0];
                site = splits.length > 1 ? splits[1] : "http://www.asos.com/";
                if (!site.toLowerCase().startsWith("http")) {
                    site = "http://" + site;
                }
                System.out.println(SDF.format(new Date()) + ": The uri is " + uri);
                System.out.println(SDF.format(new Date()) + ": The site is " + site);
//
//                Socket socket = null;
//                boolean reachable = false;
//                try {
//                    if (!site.toLowerCase().startsWith("http")) {
//                        site = "http://" + site;
//                    }
//                    socket = new Socket(site, 80);
//                    reachable = true;
//                    System.out.println(SDF.format(new Date()) + ": The site is OK.");
//                } catch (Exception e) {
//                    System.err.println(SDF.format(new Date()) + ": The site " + site + " is unreachable.");
//                } finally {
//                    if (socket != null) {
//                        try {
//                            socket.close();
//                        } catch (IOException e) {
//                        }
//                    }
//                }
//
//                if (!reachable) {
//                    System.err.println(SDF.format(new Date()) + ": As the site is unreachable, it is replaced by ASOS. ImageUrl = " + uri);
//                    site = "http://www.asos.com/";
//                }

                // Put at most 1000 images in a temporary HIB, estimated max about 500MB per DB
                if (i >= iprev + 1000) {
                    hib.close();
                    context.write(new BooleanWritable(true), new Text(hib.getPath().toString()));
//                    tempPath = conf.get("downloader.outpath") + i + ".hib.tmp";

                    tempPath = distDestDir + key.get() + "_" + attemptId + "_" + i + ".dist.hib";
                    createdHibPaths.add(tempPath);
//                    tempPath = conf.get("downloader.outpath") + i + ".dist.hib";
                    hib = new HipiImageBundle(new Path(tempPath), conf);
                    hib.open(HipiImageBundle.FILE_MODE_WRITE, true);
                    iprev = i;
                }

                // Setup to time download
                long startT = 0;
                long stopT = 0;
                startT = System.currentTimeMillis();

                // Perform download and update HIB
                try {

                    String type = "";
                    URLConnection conn;

                    // Attempt to download image at URL using java.net
                    try {
                        URL link = new URL(uri);
                        System.out.println(SDF.format(new Date()) + ": Downloading " + link.toString());
                        conn = link.openConnection();
                        conn.connect();
                        type = conn.getContentType();
                    } catch (Exception e) {
                        System.err.println(SDF.format(new Date()) + ": Connection error while trying to download: " + uri);
                        continue;
                    }

                    // Check that image format is supported, header is parsable, and add to HIB if so
                    if (type != null && (type.compareTo("image/jpeg") == 0)) {  // Skip PNG as it require too much memory in large scale
//                    if (type != null && (type.compareTo("image/jpeg") == 0 || type.compareTo("image/png") == 0)) {

                        // Get input stream for URL connection
                        InputStream bis = new BufferedInputStream(conn.getInputStream());

                        // Mark current location in stream for later reset
                        bis.mark(Integer.MAX_VALUE);

                        // Attempt to decode the image header
                        ImageHeader header = (type.compareTo("image/jpeg") == 0 ? JPEGImageUtil.getInstance().decodeImageHeader(bis) : PNGImageUtil.getInstance().decodeImageHeader(bis));

                        if (header == null) {
                            System.err.println(SDF.format(new Date()) + ": Failed to parse header, not added to HIB: " + uri);
//                        } else if (type.compareTo("image/jpeg") != 0){
//                            System.out.println("Skipped as only jpg is supported now: " + uri);
//                            bis.close();
                        } else {

                            // Passed header decode test, so reset to beginning of stream
                            bis.reset();

                            // hipi's PNGImageUtil cannot decode PNG in large scale. Heap size issue
                            if (type.compareTo("image/jpeg") != 0) {
                                FloatImage decodedPngFloatImage = hipi.image.io.PNGImageUtil.getInstance().decodeImage(bis);

                                ByteArrayOutputStream jpgOS = new ByteArrayOutputStream();
                                hipi.image.io.JPEGImageUtil.getInstance().encodeImage(decodedPngFloatImage, header, jpgOS);

//                                // Image Type Conversion refer to org.apache.commons.imaging.formats.png --> ConvertPngToGifTest
//                                Map<String, Object> params = new HashMap<String, Object>();
//                                BufferedImage bufferedImage = Imaging.getBufferedImage(bis, params);
//                                ByteArrayOutputStream jpgOS = new ByteArrayOutputStream();
//                                Imaging.writeImage(bufferedImage, jpgOS, ImageFormats.JPEG,params);
                                byte[] jpgData = jpgOS.toByteArray();
                                //bufferedImage.flush();
                                jpgOS.close();
                                bis.close();

                                //ByteArrayInputStream jpgIS = new ByteArrayInputStream(jpgData);
                                bis = new ByteArrayInputStream(jpgData);
                            }

                            ByteArrayOutputStream jpgWithExifOS = new ByteArrayOutputStream();
                            //File outputFile = new File(localTempDirStr,localTempFilenameStr);
                            this.AddUrlInJpgExifSiteMetadata(bis, null, jpgWithExifOS, uri, site);
//                            this.UpdateUrlInExifMetadata(jpgIS, null, jpgWithExifOS, uri);
                            bis.close();

                            byte[] jpgWithExifData = jpgWithExifOS.toByteArray();
                            //jpgIS.close();
                            jpgWithExifOS.close();
                            ByteArrayInputStream jpgWithExifIS = new ByteArrayInputStream(jpgWithExifData);
//                            byte[] data = output.toByteArray();
//                            output.close();
//                            ByteArrayInputStream input = new ByteArrayInputStream(data);
                            //InputStream input = new FileInputStream(outputFile);
                            // Add image to HIB
//                            hib.addImage(jpgWithExifIS, type.compareTo("image/jpeg") == 0 ? ImageType.JPEG_IMAGE : ImageType.PNG_IMAGE);
                            hib.addImage(jpgWithExifIS, ImageType.JPEG_IMAGE);  // ONLY ACCEPT JPG here
                            jpgWithExifIS.close();
//                            // Joe: to enable adding our preferred EXIF info, use FloatImage here
//                            ImageDecoder imgDecoder = type.compareTo("image/jpeg") == 0 ? JPEGImageUtil.getInstance() : PNGImageUtil.getInstance();
//                            ImageEncoder imgEncoder = type.compareTo("image/jpeg") == 0 ? JPEGImageUtil.getInstance() : PNGImageUtil.getInstance();
//                            FloatImage img = imgDecoder.decodeImage(bis);
//
//                            // Joe: Standard Size Limit
//                            Integer widthLimit = conf.getInt("dragonfly.rescale.width", 0);
//                            Integer heightLimit = conf.getInt("dragonfly.rescale.height", 0);
//                            int pixelLimit = heightLimit * widthLimit;
//                            if (pixelLimit > 0) {
//                                int imgSize = img.getHeight() * img.getWidth();
//                                float ratio = pixelLimit / imgSize;
//                                if (ratio < 1.0f) {
//                                    img.scale(ratio);
//                                }
//                            }
//
//                            // Test
//                            //final ImageMetadata metadata = Imaging.getMetadata(jpegImageFile);
//                            System.out.println("Try Add EXIF to header as img_url=" + uri);
//                            header.addEXIFInformation("img_url", uri);
//                            System.out.println("Get EXIF from header, img_url=" + header.getEXIFInformation("img_url"));
//
//                            // This code is found not using the header, thus persisting the image WITHOUT ANY EXIF info
//                            hib.addImage(img, imgEncoder, header);

                            System.out.println(SDF.format(new Date()) + ": Added to HIB: " + uri);

                        }
                    } else {
                        System.err.println(SDF.format(new Date()) + ": Unrecognized HTTP content type or unsupported image format [" + type + "], not added to HIB: " + uri);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    System.err.println(SDF.format(new Date()) + ": Encountered network error while trying to download: " + uri);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }

                i++;

                // Report success and elapsed time
                stopT = System.currentTimeMillis();
                float el = (float) (stopT - startT) / 1000.0f;
                System.out.println(SDF.format(new Date()) + ": > Time elapsed " + el + " seconds");
            }

            try {

                // Output key/value pair to reduce layer consisting of boolean and path to HIB
                context.write(new BooleanWritable(true), new Text(hib.getPath().toString()));

                // Cleanup
                reader.close();
                hib.close();

            } catch (Exception e) {
                e.printStackTrace();
            }

            isCompleted = true;
        }

        /**
         * To update our custom URL tag in image. Customized according to
         * org.apache.commons.imaging.examples For HIPI, Use
         * ImageHeader.getEXIFInformation("Unknown tag (0x84e0)") to get the
         * value
         *
         * @param jpegImageFile
         * @param dst
         * @throws IOException
         * @throws ImageReadException
         * @throws ImageWriteException
         */
        public void AddUrlInJpgExifSiteMetadata(InputStream is, String filename, ByteArrayOutputStream output, String img_url, String site)
                //        public void UpdateUrlInExifMetadata(InputStream is, String filename, final File dst, String img_url)
                throws IOException, ImageReadException, ImageWriteException {
//        OutputStream os = null;
//        boolean canThrow = false;
//        try {
            TiffOutputSet outputSet = null;

            // note that metadata might be null if no metadata is found.
            final ImageMetadata metadata = Imaging.getMetadata(is, filename);
            final JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
            if (null != jpegMetadata) {
                // note that exif might be null if no Exif metadata is found.
                final TiffImageMetadata exif = jpegMetadata.getExif();

                if (null != exif) {
                    // TiffImageMetadata class is immutable (read-only).
                    // TiffOutputSet class represents the Exif data to write.
                    //
                    // Usually, we want to update existing Exif metadata by
                    // changing
                    // the values of a few fields, or adding a field.
                    // In these cases, it is easiest to use getOutputSet() to
                    // start with a "copy" of the fields read from the image.
                    outputSet = exif.getOutputSet();
                }
            }

            // if file does not contain any exif metadata, we create an empty
            // set of exif metadata. Otherwise, we keep all of the other
            // existing tags.
            if (null == outputSet) {
                outputSet = new TiffOutputSet();
            }

            {
                // Example of how to add a field/tag to the output set.
                //
                // Note that you should first remove the field/tag if it already
                // exists in this directory, or you may end up with duplicate
                // tags. See above.
                //
                // Certain fields/tags are expected in certain Exif directories;
                // Others can occur in more than one directory (and often have a
                // different meaning in different directories).
                //
                // TagInfo constants often contain a description of what
                // directories are associated with a given tag.
                //
                final TiffOutputDirectory exifDirectory = outputSet
                        .getOrCreateExifDirectory();
                // make sure to remove old value if present (this method will
                // not fail if the tag does not exist).
                exifDirectory.removeField(ExifTagConstants.EXIF_TAG_SITE);
                exifDirectory.add(ExifTagConstants.EXIF_TAG_SITE, img_url);

                exifDirectory.removeField(ExifTagConstants.EXIF_TAG_OWNER_NAME);
                exifDirectory.add(ExifTagConstants.EXIF_TAG_OWNER_NAME, site);
            }

            // printTagValue(jpegMetadata, TiffConstants.TIFF_TAG_DATE_TIME);
            //os = new FileOutputStream(dst);
            //os = new BufferedOutputStream(os);
            // Passed header decode test, so reset to beginning of stream
            is.reset();
            new ExifRewriter().updateExifMetadataLossless(is, output, outputSet);

//            canThrow = true;
//        } finally {
//            IoUtils.closeQuietly(canThrow, os);
//        }
        }
    }

    public static class DownloaderReducer extends
            Reducer<BooleanWritable, Text, BooleanWritable, Text> {

        private static Configuration conf;
        private FileSystem hdfs;

        @Override
        public void setup(Context context) throws IOException, InterruptedException {
            this.conf = context.getConfiguration();
            hdfs = FileSystem.get(conf);
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            super.cleanup(context); //To change body of generated methods, choose Tools | Templates.
            hdfs.close();
        }

        // Combine HIBs produced by the map tasks into a single HIB
        @Override
        public void reduce(BooleanWritable key, Iterable<Text> values, Context context)
                throws IOException, InterruptedException {

            if (key.get()) {

                // Get path to output HIB
                Path outputHibPath = new Path(conf.get("downloader.outfile"));

                // Create HIB for writing
                HipiImageBundle hib = new HipiImageBundle(outputHibPath, conf);
                hib.open(HipiImageBundle.FILE_MODE_WRITE, true);

//                Boolean preserveAsDistributed = conf.getBoolean("dragonfly.preserveAsDistributed", false);
//                String distDestDir = "";
//                if (preserveAsDistributed) {
//                    distDestDir = conf.get("downloader.outfile") + "_dist";
//                    fileSystem.mkdirs(new Path(distDestDir));
//                }
                // Iterate over the temporary HIB files created by map tasks
                int part = 0;
                for (Text tempString : values) {
                    Path tempPath = new Path(tempString.toString());

                    FileStatus fileStatus = hdfs.getFileStatus(tempPath);
                    if (fileStatus.getLen() == 0) {
                        try {
                            System.out.println(SDF.format(new Date()) + ": Empty hib " + tempPath.toString() + " will be removed.");
                            hdfs.delete(tempPath, false);
                            hdfs.delete(new Path(tempPath.toString() + ".dat"), false);
                            continue;
                        } catch (Exception e) {
                            System.out.println(SDF.format(new Date()) + ": Warning: Empty hib " + tempPath.toString() + " cannot be removed.");
                            continue;
                        }
                    }
                    // Open the temporary HIB file
                    HipiImageBundle inputBundle = new HipiImageBundle(tempPath, conf);

                    // Append temporary HIB file to output HIB (this is fast)
                    hib.append(inputBundle);

//                    Path indexPath = inputBundle.getPath();
//                    Path dataPath = new Path(indexPath.toString() + ".dat");
//                    Text outputPath = null;
//                    if (preserveAsDistributed) {
//                        // Rename temporary HIB (both .hib and .hib.dat files) to enable distributed Indexing
//                        String distPath = distDestDir + "/" + part;
//                        fileSystem.rename(indexPath, (new Path(distDestDir + "/" + indexPath.getName())));
//                        fileSystem.rename(dataPath, (new Path(distDestDir + "/" + dataPath.getName())));
//
//                        // Emit output key/value pair indicating temporary HIB has been processed
//                        outputPath = new Text(distPath);
//                    } else {
//                        // (Default) Remove temporary HIB (both .hib and .hib.dat files)
//                        fileSystem.delete(indexPath, false);
//                        fileSystem.delete(dataPath, false);
//
//                    }
                    // Emit output key/value pair indicating temporary HIB has been processed
                    Text outputPath = new Text(inputBundle.getPath().toString());
                    context.write(new BooleanWritable(true), outputPath);
                    context.progress();

                    part++;
                }

                Boolean preserveAsDistributed = conf.getBoolean("dragonfly.preserveAsDistributed", false);
                if (!preserveAsDistributed) {
                    String distDestDir = conf.get("downloader.outfile") + "_dist/";
                    removeDir(distDestDir, hdfs);
                }

                // Finalize output HIB
                hib.close();

            }
        }
    }

    private static void removeDir(String path, FileSystem fs) throws IOException {
        Path output_path = new Path(path);
//        FileSystem fs = FileSystem.get(conf);
        if (fs.exists(output_path)) {
            fs.delete(output_path, true);
        }
    }

    public int run(String[] args) throws Exception {
        //Configuration conf = new Configuration();
        Configuration conf = super.getConf();

        if (conf.get("downloader.outfile") == null || conf.get("downloader.inputPath") == null) {
            System.err.println("Please define downloader.outfile & downloader.inputPath EVEN you want to use Solr");
            System.exit(0);
        }
//
//        if (conf.get("downloader.inputPath") == null && conf.get("downloader.solrUrl") == null) {
//            System.err.println("Please define EITHER downloader.inputPath OR downloader.solrUrl");
//            System.exit(0);
//        }

        String solrUrl = conf.get("downloader.solrUrl");
        int solrStartAt = conf.getInt("downloader.solrStartAt", 0);
        int solrMaxCount = conf.getInt("downloader.solrMaxCount", 0);
        String inputFile = conf.get("downloader.inputPath");
        String outputFile = conf.get("downloader.outfile");
        String outputPath = outputFile.substring(0, outputFile.lastIndexOf('/') + 1);
        conf.setStrings("downloader.outpath", outputPath);
        int nodes = conf.getInt("downloader.nodeNum", 5);
        if (nodes < 1) {
            nodes = 5;
            conf.setInt("downloader.nodeNum", nodes);
        }

        System.out.println(SDF.format(new Date()) + ":INFO: solrUrl = " + solrUrl);
        System.out.println(SDF.format(new Date()) + ":INFO: inputFile = " + inputFile);
        System.out.println(SDF.format(new Date()) + ":INFO: outputFile = " + outputFile);
        System.out.println(SDF.format(new Date()) + ":INFO: outputPath = " + outputPath);
        System.out.println(SDF.format(new Date()) + ":INFO: nodes = " + nodes);

        // Referenced Guideline and Library:
        // https://github.com/FasterXML/jackson-core
        // http://www.studytrails.com/java/json/java-jackson-json-streaming.jsp
        if (solrUrl != null && solrUrl.toLowerCase().startsWith("http")) {
            System.out.println(SDF.format(new Date()) + ":INFO: Enter Solr Process 1: Validation");
            String url = solrUrl;// + "&start=0&rows=1";
            // get an instance of the json parser from the json factory
            JsonFactory factory = new JsonFactory();
            JsonParser parser = factory.createParser(new URL(url));

            int statusCode = -1;
            int numFound = -1;
            while (!parser.isClosed() && (statusCode == -1 || numFound == -1)) {
                // get the token
                JsonToken token = parser.nextToken();
                // if its the last token then we are done
                if (token == null) {
                    System.err.println("By SolrUrl, Nothing is gained.");
                    System.exit(0);
                }
                //System.out.println("INFO: ParserValue=" + parser.getText());

                if (parser.getCurrentLocation().getLineNr() >= 25) {
                    System.err.println("SolrUrl is defined but the status code and numFound cannot be found within 25 lines, which seems invalid format.");
                    System.exit(0);
                }

                if (JsonToken.FIELD_NAME.equals(token) && "responseHeader".equals(parser.getCurrentName())) {
                    // we are entering the responseHeader now. The first token should be Start of Object
                    token = parser.nextToken();
                    if (!JsonToken.START_OBJECT.equals(token)) {
                        System.err.println("By SolrUrl, responseHeader should be Object.");
                        System.exit(0);
                        //break;
                    }
                    while (true) {
                        token = parser.nextToken();
                        if (token == null) {
                            break;
                        }

                        //System.out.println("INFO: ParserValue=" + parser.getText());
                        if (JsonToken.FIELD_NAME.equals(token) && "status".equals(parser.getCurrentName())) {
                            token = parser.nextToken();
                            if (token.isNumeric()) {
                                statusCode = parser.getIntValue();
                                break;
                            } else {
                                System.err.println("By SolrUrl, statusCode is reached but it is not numeric. value = " + parser.getText());
                                System.exit(0);
                            }
                        }
                    }

                    if (statusCode != 0) {
                        System.err.println("By SolrUrl, statusCode is not 0, i.e. invalid Query. statusCode = " + statusCode);
                        System.exit(0);
                    }
                }

                if (JsonToken.FIELD_NAME.equals(token) && "response".equals(parser.getCurrentName())) {
                    // we are entering the response now. The first token should be Object
                    token = parser.nextToken();
                    if (!JsonToken.START_OBJECT.equals(token)) {
                        System.err.println("By SolrUrl, response should be Object.");
                        System.exit(0);
                    }
                    while (true) {
                        token = parser.nextToken();
                        if (token == null) {
                            break;
                        }
                        //System.out.println("INFO: ParserValue=" + parser.getText());
                        if (JsonToken.FIELD_NAME.equals(token) && "numFound".equals(parser.getCurrentName())) {
                            token = parser.nextToken();
                            if (token.isNumeric()) {
                                numFound = parser.getIntValue();
                                break;
                            } else {
                                System.err.println("By SolrUrl, numFound is reached but it is not numeric. value = " + parser.getText());
                                System.exit(0);
                            }
                        }
                    }

                    if (numFound <= 0) {
                        System.err.println("By SolrUrl, numFound is not >0, i.e. empty or invalid dataset. numFound = " + numFound);
                        System.exit(0);
                    }
                }
            }

            parser.close();
            if (solrMaxCount != 0){
                numFound = Math.min(solrMaxCount, numFound);
            }
            
            System.out.println(SDF.format(new Date()) + ":INFO: After Validation, " + numFound + " images should be retrieved.");
            url = solrUrl + "&start=" + solrStartAt + "&rows=" + numFound;
            System.out.println(SDF.format(new Date()) + ":INFO: Real Retrival URL = " + url);
            Map<String, String> pairs = new HashMap<String, String>();
            parser = factory.createParser(new URL(url));
            while (!parser.isClosed()) {
                // get the token
                JsonToken token = parser.nextToken();
                // if its the last token then we are done
                if (token == null) {
                    System.err.println("By SolrUrl, Nothing is gained.");
                    System.exit(0);
                }
                //System.out.println("DEBUG: A " + parser.getText());

                if (JsonToken.FIELD_NAME.equals(token) && "responseHeader".equals(parser.getCurrentName())) {
                    // we are entering the responseHeader now. The first token should be Start of Object
                    token = parser.nextToken();
                    //System.out.println("DEBUG: B " + parser.getText());
                    if (!JsonToken.START_OBJECT.equals(token)) {
                        System.err.println("By SolrUrl, responseHeader should be Object.");
                        System.exit(0);
                    }
                    while (true) {
                        token = parser.nextToken();
                        if (token == null) {
                            break;
                        }
                        //System.out.println("INFO: ParserValue=" + parser.getText());
                        if (JsonToken.FIELD_NAME.equals(token) && "status".equals(parser.getCurrentName())) {
                            token = parser.nextToken();
                            if (token.isNumeric()) {
                                statusCode = parser.getIntValue();
                                break;
                            } else {
                                System.err.println("By SolrUrl, statusCode is reached but it is not numeric. value = " + parser.getText());
                                System.exit(0);
                            }
                        }
                    }

                    if (statusCode != 0) {
                        System.err.println("By SolrUrl, statusCode is not 0, i.e. invalid Query. statusCode = " + statusCode);
                        System.exit(0);
                    }
                }

                //System.out.println("DEBUG: C " + parser.getText());
                if (JsonToken.FIELD_NAME.equals(token) && "response".equals(parser.getCurrentName())) {
                    while (true) {
                        token = parser.nextToken();
                        //System.out.println("DEBUG: D " + parser.getText());
                        if (!JsonToken.START_OBJECT.equals(token)) {
                            break;
                        }
                        //System.out.println("DEBUG: D2 " + parser.getText());
                        while (true) {
                            token = parser.nextToken();
                            //System.out.println("DEBUG: E " + parser.getText());
                            if (token == null) {
                                break;
                            }
                            //System.out.println("INFO: 1 ParserValue=" + parser.getText());
                            if (JsonToken.FIELD_NAME.equals(token) && "docs".equals(parser.getCurrentName())) {
                                token = parser.nextToken();
                                if (token == null) {
                                    break;
                                }
                                if (!JsonToken.START_ARRAY.equals(token)) {
                                    System.err.println("By SolrUrl, docs should be Array.");
                                    System.exit(0);
                                    //break;
                                }
                                //System.out.println("INFO: OK it is array " + parser.getText());
                                while (true) {
                                    token = parser.nextToken();
                                    if (token == null) {
                                        break;
                                    }
                                    //System.out.println("INFO: 2 ParserValue=" + parser.getText());
                                    if (!JsonToken.START_OBJECT.equals(token)) {
                                        System.err.println("By SolrUrl, items within docs should be Object.");
                                        System.exit(0);
                                        //break;
                                    }

                                    String domain = "";
                                    String img_url = "";
                                    while (true) {
                                        token = parser.nextToken();
                                        if (token == null) {
                                            break;
                                        }
                                        if (!JsonToken.END_OBJECT.equals(token)) {

                                            if (!domain.isEmpty() && !img_url.isEmpty()) {
                                                //System.out.println("INFO: Current ImgUrl = " + img_url + " & Domain = " + domain);
                                                pairs.putIfAbsent(img_url, domain);
                                            }
                                        }
                                        //System.out.println("INFO: 3 ParserValue=" + parser.getText());
                                        if (JsonToken.FIELD_NAME.equals(token) && ("domain".equals(parser.getCurrentName()) || "host".equals(parser.getCurrentName()))) {
                                            token = parser.nextToken();
                                            domain = parser.getText();
                                        }
                                        if (JsonToken.FIELD_NAME.equals(token) && ("id".equals(parser.getCurrentName()) || "url".equals(parser.getCurrentName()))) {
                                            token = parser.nextToken();
                                            img_url = parser.getText();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (numFound <= 0) {
                        System.err.println("By SolrUrl, numFound is not >0, i.e. empty or invalid dataset. numFound = " + numFound);
                        System.exit(0);
                    }
                }
            }

            if (pairs.isEmpty()) {
                System.err.println("The result list from Solr is empty.");
                System.exit(0);
            }

            StringBuilder contentBuilder = new StringBuilder();
            System.out.println(SDF.format(new Date()) + ":INFO: There are " + pairs.size() + " images gained from Solr");
            //Pattern pureNumberPattern = Pattern.compile("^.+\\.dist\\.hib$");
            for (Map.Entry<String, String> entry : pairs.entrySet()) {
                contentBuilder.append(entry.getKey() + "|" + entry.getValue() + "\n");
            }
//
            Path pathList = new Path(inputFile);
            System.out.println(SDF.format(new Date()) + ":Try to write the fileList to HDFS " + inputFile);
            FileSystem hdfs = FileSystem.get(conf);
            if (hdfs.isFile(pathList)) {
                hdfs.delete(pathList, true);
            }
            BufferedWriter br = new BufferedWriter(new OutputStreamWriter(hdfs.create(pathList, true)));
            System.out.println(SDF.format(new Date()) + ":BufferedWriter initialized");
            br.write(contentBuilder.toString());
            br.close();
            System.out.println(SDF.format(new Date()) + ":Write finish!");
        }
//        System.out.println("For Testing Purpose, Stop here no matter what");
//        System.exit(0);

//    Job job = Job.getInstance(conf, "Downloader");
        Job job = Job.getInstance(conf, conf.get("mapreduce.job.name", "Downloader"));  //If param is null, use "Downloader"
        job.setJarByClass(Downloader.class);
        job.setMapperClass(DownloaderMapper.class);
        job.setReducerClass(DownloaderReducer.class);
        job.setInputFormatClass(DownloaderInputFormat.class);
        job.setOutputKeyClass(BooleanWritable.class);
        job.setOutputValueClass(Text.class);
        job.setNumReduceTasks(1);

        // Cannot afford multiple attempts
        job.setMaxMapAttempts(1);
        job.setMaxReduceAttempts(1);

        FileSystem hdfs = FileSystem.get(conf);
        Boolean isCleanStart = conf.getBoolean("dragonfly.cleanStart", false);
        if (isCleanStart) {
            removeDir(outputFile + "_output", hdfs);
        }

        FileOutputFormat.setOutputPath(job, new Path(outputFile + "_output"));

        DownloaderInputFormat.setInputPaths(job, new Path(inputFile));

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Downloader(), args);
        System.exit(res);
    }
}
