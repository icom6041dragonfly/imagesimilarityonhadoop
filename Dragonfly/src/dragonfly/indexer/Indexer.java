package dragonfly.indexer;

import hipi.image.FloatImage;
import hipi.image.ImageHeader;
import hipi.image.io.ImageEncoder;
import hipi.image.io.JPEGImageUtil;
import hipi.imagebundle.HipiImageBundle;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
//import java.nio.file.Path;

import net.semanticmetadata.lire.impl.ChainedDocumentBuilder;
import net.semanticmetadata.lire.impl.GenericDocumentBuilder;

import net.semanticmetadata.lire.imageanalysis.CEDD;            //ColorEdge
import net.semanticmetadata.lire.imageanalysis.FCTH;            //Fuzzy Color and Texture
import net.semanticmetadata.lire.imageanalysis.PHOG;            //Pattern Search
import net.semanticmetadata.lire.imageanalysis.ColorLayout;     //Color Search

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
//import javax.imageio.ImageIO;
import net.semanticmetadata.lire.DocumentBuilder;
import net.semanticmetadata.lire.utils.LuceneUtils;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.io.BooleanWritable;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.FSDirectory;

public class Indexer extends Configured implements Tool {

    final static SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//    public static class IndexerMapper extends Mapper<ImageHeader, FloatImage, IntWritable, Text> {
    public static class IndexerMapper extends Mapper<IntWritable, Text, BooleanWritable, Text> {

        public Path path;
        private static Configuration conf;

        @Override
        public void setup(Context context) throws IOException, InterruptedException {
            System.out.println(SDF.format(new Date()) + ":Setup Mapper");  //debug
            this.conf = context.getConfiguration();
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            System.out.println(SDF.format(new Date()) + ":Clean-up Mapper " + context.getTaskAttemptID().toString());  //debug
            System.out.println(SDF.format(new Date()) + ":Mapper Status = " + context.getStatus());  //debug
            System.out.println(SDF.format(new Date()) + ":Mapper Progress = " + context.getProgress());  //debug
            System.out.println(SDF.format(new Date()) + ":Mapper need Cleanup = " + context.getTaskCleanupNeeded());  //debug
            super.cleanup(context); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void map(IntWritable key, Text value, Context context) throws IOException, InterruptedException {

            System.out.println("Start Mapper");  //debug
            System.out.println("Key = " + key.toString());  //debug
            System.out.println("value = " + value.toString());  //debug

            // Use multiple DocumentBuilder instances:
            ChainedDocumentBuilder docBuilder = new ChainedDocumentBuilder();
            docBuilder.addBuilder(new GenericDocumentBuilder(CEDD.class));
            docBuilder.addBuilder(new GenericDocumentBuilder(FCTH.class));
            docBuilder.addBuilder(new GenericDocumentBuilder(PHOG.class));
            docBuilder.addBuilder(new GenericDocumentBuilder(ColorLayout.class));
            //docBuilder.addBuilder(new GenericDocumentBuilder(AutoColorCorrelogram.class)); //AutoColor throw java.lang.ArrayIndexOutOfBoundsException at java.awt.image.ComponentSampleModel.getPixel()

//            System.out.println("!!! Print the Whole Conf in Mapper !!!");  //debug
//            conf.writeXml(System.out);
//            System.out.println("!!! Print Done !!!");  //debug
            FileSystem localfs = FileSystem.getLocal(conf);

            String tempLocalIndexPathStr = java.nio.file.Paths.get(conf.get("indexer.localTempPath"), "localTmpIndex_" + key + "_" + context.getTaskAttemptID().getId()).toString();
            //Path tempLocalIndexPath = Path.mergePaths(new Path(conf.get("indexer.localTempPath")), new Path("localTmpIndex_" + key));//. + "_tmp_" + key;

            removeDir(tempLocalIndexPathStr, localfs);
            localfs.mkdirs(new Path(tempLocalIndexPathStr));
            // Creating an Lucene IndexWriter
            IndexWriterConfig indexWriterconf = new IndexWriterConfig(LuceneUtils.LUCENE_VERSION,
                    new WhitespaceAnalyzer());

            IndexWriter indexWriter = new IndexWriter(FSDirectory.open(new File(tempLocalIndexPathStr)), indexWriterconf);
            System.out.println(SDF.format(new Date()) + ":Lucene Index Dir Created at " + tempLocalIndexPathStr);  //debug

            // The value argument contains a list of HIB paths delimited by \n. Setup buffered reader to allow processing this string line by line.
            BufferedReader reader = new BufferedReader(new StringReader(value.toString()));
            String hibPathStr;

            System.out.println(SDF.format(new Date()) + ":BufferedReader to Hib path created");  //debug

            int indexedImgCount = 0;
            Boolean hasFileProcessed = false;
            // Iterate through HIBs
            while ((hibPathStr = reader.readLine()) != null) {
                try {
                    hasFileProcessed = true;
                    HipiImageBundle hib = new HipiImageBundle((new Path(hibPathStr)), conf);

                    hib.open(HipiImageBundle.FILE_MODE_READ);
                    System.out.println(SDF.format(new Date()) + ":hib opened: " + hibPathStr);  //debug
                    ImageHeader imgHeader;
                    while ((imgHeader = hib.next()) != null) {
                        String imgUrl = imgHeader.getEXIFInformation("Unknown tag (0x84e0)");
                        String siteUrl = imgHeader.getEXIFInformation("Owner Name");
                        System.out.println(SDF.format(new Date()) + ":image read: " + imgUrl);  //debug
                        if (imgHeader.height * imgHeader.width < 200 * 300) {
                            System.out.println(SDF.format(new Date()) + ":image H x W = " + imgHeader.height + "x" + imgHeader.width + ", smaller than our 200x300 request. Skip " + imgUrl);  //debug
                            continue;
                        }

                        FloatImage floatImg = hib.getCurrentImage();
                        // Encoding Method reference to http://stackoverflow.com/questions/17451918/get-a-bufferedimage-from-and-outputstream
                        System.out.println(SDF.format(new Date()) + ":Try change FloatImage to BufferedImage via ByteArrayInput/OutputStream");  //debug
                        ByteArrayOutputStream output = new ByteArrayOutputStream();
                        ImageEncoder imgEncoder = JPEGImageUtil.getInstance();
                        imgEncoder.encodeImage(floatImg, imgHeader, output);
                        System.out.println(SDF.format(new Date()) + ":FloatImage encoded to JPEG ByteArrayOutputStream");  //debug
                        byte[] data = output.toByteArray();
                        output.close();
                        System.out.println(SDF.format(new Date()) + ":ByteArrayOutputStream closed");  //debug

                        System.out.println(SDF.format(new Date()) + ":ByteArrayOutputStream to byte[] done");  //debug
                        ByteArrayInputStream input = new ByteArrayInputStream(data);
                        System.out.println(SDF.format(new Date()) + ":ByteArrayInputStream ready");  //debug
                        BufferedImage img = null;
                        try {
                            img = Imaging.getBufferedImage(input);
                        } catch (ImageReadException ex) {
                            System.err.println("Image " + imgUrl + " cannot be converted into bufferedImage. Skip and continue.");
                            Logger.getLogger(Indexer.class.getName()).log(Level.SEVERE, null, ex);
                            input.close();
                            continue;
                        }
                        input.close();

                        int sampleW = Math.round(0.05f * imgHeader.width) + 1;
                        int sampleH = Math.round(0.05f * imgHeader.height) + 1;
                        // Take the upper-left pixel
                        float edgePixelR = floatImg.getPixel(0, Math.round(0.3f * imgHeader.height), 0) * 255;
                        float edgePixelG = floatImg.getPixel(0, Math.round(0.3f * imgHeader.height), 1) * 255;
                        float edgePixelB = floatImg.getPixel(0, Math.round(0.3f * imgHeader.height), 2) * 255;
                        System.out.println(SDF.format(new Date()) + ":Edge RGB is " + edgePixelR + ", " + edgePixelG + ", " + edgePixelB);  //debug
                        Color edgeColor = averageColor(img, 0, Math.round(0.3f * imgHeader.height), sampleW, sampleH);

                        // Take the upper-center pixel
                        float upperCenterPixelR = floatImg.getPixel(Math.round(0.5f * imgHeader.width), Math.round(0.3f * imgHeader.height), 0) * 255;
                        float upperCenterPixelG = floatImg.getPixel(Math.round(0.5f * imgHeader.width), Math.round(0.3f * imgHeader.height), 1) * 255;
                        float upperCenterPixelB = floatImg.getPixel(Math.round(0.5f * imgHeader.width), Math.round(0.3f * imgHeader.height), 2) * 255;
                        System.out.println(SDF.format(new Date()) + ":Upper-center RGB is " + upperCenterPixelR + ", " + upperCenterPixelG + ", " + upperCenterPixelB);  //debug
                        Color upperCenterColor = averageColor(img, Math.round(0.425f * imgHeader.width), Math.round(0.3f * imgHeader.height), sampleW, sampleH);

    //                    float centerPixelR = floatImg.getPixel(Math.round(0.5f*imgHeader.width), Math.round(0.5f*imgHeader.height), 0) * 255;
                        //                    float centerPixelG = floatImg.getPixel(Math.round(0.5f*imgHeader.width), Math.round(0.5f*imgHeader.height), 1) * 255;
                        //                    float centerPixelB = floatImg.getPixel(Math.round(0.5f*imgHeader.width), Math.round(0.5f*imgHeader.height), 2) * 255;
                        // Take the lower-center pixel
                        float lowerCenterPixelR = floatImg.getPixel(Math.round(0.5f * imgHeader.width), Math.round(0.75f * imgHeader.height), 0) * 255;
                        float lowerCenterPixelG = floatImg.getPixel(Math.round(0.5f * imgHeader.width), Math.round(0.75f * imgHeader.height), 1) * 255;
                        float lowerCenterPixelB = floatImg.getPixel(Math.round(0.5f * imgHeader.width), Math.round(0.75f * imgHeader.height), 2) * 255;
                        System.out.println(SDF.format(new Date()) + ":Lower-center RGB is " + lowerCenterPixelR + ", " + lowerCenterPixelG + ", " + lowerCenterPixelB);  //debug
                        Color lowerCenterColor = averageColor(img, Math.round(0.425f * imgHeader.width), Math.round(0.75f * imgHeader.height), sampleW, sampleH);

                        float upperLowerDiffR = Math.abs(upperCenterColor.getRed() - lowerCenterColor.getRed());
                        float upperLowerDiffG = Math.abs(upperCenterColor.getGreen() - lowerCenterColor.getGreen());
                        float upperLowerDiffB = Math.abs(upperCenterColor.getBlue() - lowerCenterColor.getBlue());
                        System.out.println(SDF.format(new Date()) + ":Upper vs Lower RGB diff is " + upperLowerDiffR + ", " + upperLowerDiffG + ", " + upperLowerDiffB);  //debug

                        int x = Math.round(0.4f * imgHeader.width);
                        int y = 0;
                        int w = Math.round(0.2f * imgHeader.width);
                        int h = Math.round(0.2f * imgHeader.height);

                        // Main rule: If upper-center is simiar to lower-center, take center part of the image 
                        if (upperLowerDiffR < 50
                                && upperLowerDiffG < 50
                                && upperLowerDiffB < 50
                                && (upperLowerDiffR + upperLowerDiffG + upperLowerDiffB) < 100) {
                            System.out.println(SDF.format(new Date()) + ":Center crop is taken");  //debug
                            y = Math.round(0.4f * imgHeader.height);
                        } else {
                            System.out.println(SDF.format(new Date()) + ":Upper vs Lower diff is " + (upperLowerDiffR + upperLowerDiffG + upperLowerDiffB) + ", >=50. So continue selection...");  //debug
                            float upperEdgeDiffR = Math.abs(upperCenterColor.getRed() - edgeColor.getRed());
                            float upperEdgeDiffG = Math.abs(upperCenterColor.getGreen() - edgeColor.getGreen());
                            float upperEdgeDiffB = Math.abs(upperCenterColor.getBlue() - edgeColor.getBlue());
                            System.out.println(SDF.format(new Date()) + ":Upper vs Edge RGB diff is " + upperEdgeDiffR + ", " + upperEdgeDiffG + ", " + upperEdgeDiffB);  //debug

                            float lowerEdgeDiffR = Math.abs(lowerCenterColor.getRed() - edgeColor.getRed());
                            float lowerEdgeDiffG = Math.abs(lowerCenterColor.getGreen() - edgeColor.getGreen());
                            float lowerEdgeDiffB = Math.abs(lowerCenterColor.getBlue() - edgeColor.getBlue());
                            System.out.println(SDF.format(new Date()) + ":Lower vs Edge RGB diff is " + lowerEdgeDiffR + ", " + lowerEdgeDiffG + ", " + lowerEdgeDiffB);  //debug

                            // If upper-center is more simiar to edge(usually background), take lower-center part of the image 
                            if ((upperEdgeDiffR + upperEdgeDiffG + upperEdgeDiffB) < (lowerEdgeDiffR + lowerEdgeDiffG + lowerEdgeDiffB)) {
                                System.out.println(SDF.format(new Date()) + ":Lower crop is taken");  //debug
                                y = Math.round(0.6f * imgHeader.height);
                            } else {
                                System.out.println(SDF.format(new Date()) + ":Upper crop is taken");  //debug
                                y = Math.round(0.2f * imgHeader.height);
                            }
                        }
                        img = img.getSubimage(x, y, w, h);

                        Document document = null;
                        try {
                            document = docBuilder.createDocument(img, imgUrl);
                            document.add(new StringField("site", siteUrl, Field.Store.YES));
                            System.out.println(SDF.format(new Date()) + ":Document created for " + imgUrl);  //debug
                            img.flush();
                            System.out.println(SDF.format(new Date()) + ":Image flushed");  //debug
                            input.close();
                            System.out.println(SDF.format(new Date()) + ":ByteArrayInputStream closed");  //debug

                            //indexWriter.addDocument(document);
                            //indexWriter.abortMerges();
                            indexWriter.updateDocument(new Term(DocumentBuilder.FIELD_NAME_IDENTIFIER, imgUrl), document);
                            indexedImgCount++;
                            System.out.println(SDF.format(new Date()) + ": " + indexedImgCount + " images are added to this sub-index");  //debug
                            //                        pipeout.flush();
                        } catch (Exception e) {
                            System.err.println("Error reading image or indexing it.");
                            e.printStackTrace();
                        }
                        //                    pipeout.close();

                        System.out.println("Finished indexing.");

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    System.err.println(SDF.format(new Date()) + ": Error is got when reading this HIB, thus skipping " + hibPathStr);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }
            }

            indexWriter.close();

            if (hasFileProcessed) {
                System.out.println(SDF.format(new Date()) + ":All Map process finished");

                System.out.println(SDF.format(new Date()) + ":In Mapper, indexer.outputPath is now " + conf.get("indexer.outputPath"));  //debug
                String tempIndexPath = conf.get("indexer.outputPath") + "_tmp_" + key;
                System.out.println(SDF.format(new Date()) + ":tempIndexPath = " + tempIndexPath);  //debug

                FileSystem hdfs = FileSystem.get(conf);
                System.out.println(SDF.format(new Date()) + ":Clear tempIndexPath from HDFS " + tempIndexPath);  //debug    
                removeDir(tempIndexPath, hdfs);
                System.out.println(SDF.format(new Date()) + ":Lucene subindex moved from LocalFS " + tempLocalIndexPathStr + " to HDFS " + tempIndexPath);  //debug
                hdfs.copyFromLocalFile(true, new Path(tempLocalIndexPathStr), new Path(tempIndexPath));

                context.write(new BooleanWritable(true), new Text(tempIndexPath));
                System.out.println(SDF.format(new Date()) + ":End of Mapper. Temp index Path = " + tempIndexPath);  //debug
            } else {
                System.out.println(SDF.format(new Date()) + ":This Mapper will be skipped as no input.");
                removeDir(tempLocalIndexPathStr, localfs);
                context.write(new BooleanWritable(true), new Text(""));
                System.out.println(SDF.format(new Date()) + ":End of skipped Mapper.");  //debug
            }
        }

    }

    private static void removeDir(String path, FileSystem fs) throws IOException {
        Path output_path = new Path(path);
//        FileSystem fs = FileSystem.get(conf);
        if (fs.exists(output_path)) {
            fs.delete(output_path, true);
        }
    }

    /*
     * Where bi is your image, (x0,y0) is your upper left coordinate, and (w,h)
     * are your width and height respectively
     * ref http://stackoverflow.com/questions/28162488/get-average-color-on-bufferedimage-and-bufferedimage-portion-as-fast-as-possible
     */
    public static Color averageColor(BufferedImage bi, int x0, int y0, int w, int h) {
        int x1 = x0 + w;
        int y1 = y0 + h;
        long sumr = 0, sumg = 0, sumb = 0;
        for (int x = x0; x < x1; x++) {
            for (int y = y0; y < y1; y++) {
                Color pixel = new Color(bi.getRGB(x, y));
                sumr += pixel.getRed();
                sumg += pixel.getGreen();
                sumb += pixel.getBlue();
            }
        }
        int num = w * h;
        System.out.println(SDF.format(new Date()) + ": Sum R G B is " + sumr + " " + sumg + " " + sumb);  //debug
        System.out.println(SDF.format(new Date()) + ": W x H is " + w + " x " + h + " = " + num);  //debug
        System.out.println(SDF.format(new Date()) + ": average R G B is " + sumr / num + " " + sumg / num + " " + sumb / num);  //debug
        System.out.println(SDF.format(new Date()) + ":Start reduce, i.e. merging");  //debug
        return new Color((sumr / num) / 255, (sumg / num) / 255, (sumb / num) / 255);
    }

    public static class IndexerReducer extends Reducer<BooleanWritable, Text, BooleanWritable, Text> {

        public FileSystem hdfs;
        public FileSystem localFs;
        private static Configuration conf;

        @Override
        public void setup(Context context) throws IOException, InterruptedException {
            this.conf = context.getConfiguration();
            hdfs = FileSystem.get(conf);
            localFs = FileSystem.getLocal(conf);
        }

        @Override
        public void reduce(BooleanWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

//            System.out.println("!!! Print the Whole Conf in Reducer !!!");  //debug
//            conf.writeXml(System.out);
//            System.out.println("!!! Print Done !!!");  //debug
            System.out.println(SDF.format(new Date()) + ":Start reduce, i.e. merging");  //debug
            String outputPath = conf.get("indexer.outputPath");
            String localMergedIndexPathStr = java.nio.file.Paths.get(conf.get("indexer.localTempPath"), "indexMerge").toString();

            removeDir(localMergedIndexPathStr, localFs);
            if (hdfs.exists(new Path(outputPath))) {
//                hdfs.copyToLocalFile(new Path(outputPath), new Path(localMergedIndexPathStr));
                // If the file is copied from HDFS and then modified in Local, the HDFS's CRC (checksum) will be unmathced.
                // One of the solution is remove the CRC files before uploading to HDFS;
                // But it is easier to use RawLocalFileSystem to skip copying CRC
                hdfs.copyToLocalFile(false, new Path(outputPath), new Path(localMergedIndexPathStr), true); //RawLocalFileSystem is non crc file system.So, It will not create any crc files at local.

                removeDir(outputPath + "_bak", hdfs);
                hdfs.rename(new Path(outputPath), new Path(outputPath + "_bak"));   // to prevent from destructing the original file

            } else {
                localFs.mkdirs(new Path(localMergedIndexPathStr));
            }

            // Creating an Lucene IndexWriter
            IndexWriterConfig indexWriterconf = new IndexWriterConfig(LuceneUtils.LUCENE_VERSION,
                    new WhitespaceAnalyzer());
            IndexWriter indexWriter = new IndexWriter(FSDirectory.open(new File(localMergedIndexPathStr)), indexWriterconf);
            System.out.println(SDF.format(new Date()) + ":Output Lucene Writer located at " + localMergedIndexPathStr);  //debug

            int updatedIndexCount = 0;
            for (Text tempIndexPath : values) {
                if (tempIndexPath.toString().isEmpty()) {
                    continue;
                }
                Path inputTempIndexPath = new Path(tempIndexPath.toString());

                String localSubindexPathStr = java.nio.file.Paths.get(conf.get("indexer.localTempPath"), inputTempIndexPath.getName()).toString();

                //Path localSubindexPath = Path.mergePaths(inputTempIndexPath, new Path(conf.get("indexer.localTempPath"), inputTempIndexPath.getName()));
                hdfs.moveToLocalFile(inputTempIndexPath, new Path(localSubindexPathStr));
                System.out.println(SDF.format(new Date()) + ":Lucene subindex moved from HDFS " + inputTempIndexPath.toString() + " to LocalFS " + localSubindexPathStr);  //debug

                System.out.println(SDF.format(new Date()) + ":Start process on LocalFS " + localSubindexPathStr);  //debug
                IndexReader indexReader = DirectoryReader.open(FSDirectory.open(new File(localSubindexPathStr)));
                System.out.println(SDF.format(new Date()) + ":Lucene initialized Reader on " + localSubindexPathStr);  //debug
                //indexWriter.addIndexes(indexReader);
                for (int i = 0; i < indexReader.maxDoc(); i++) {
                    System.out.println("The " + (i + 1) + " doc in Subindex read");  //debug
                    Document doc = indexReader.document(i);
                    System.out.println("The " + (i + 1) + " doc in Subindex retrieved");  //debug
                    indexWriter.updateDocument(new Term(DocumentBuilder.FIELD_NAME_IDENTIFIER, doc.get(DocumentBuilder.FIELD_NAME_IDENTIFIER)), doc);
                    System.out.println("The " + (i + 1) + " doc in Subindex added");  //debug
                    updatedIndexCount++;
                }
                //indexWriter.commit();
                System.out.println(SDF.format(new Date()) + ":All Lucene SubDoc updates committed!");  //debug

                indexReader.close();
                System.out.println(SDF.format(new Date()) + ":Lucene reader closed");  //debug
                localFs.deleteOnExit(new Path(localSubindexPathStr));
                System.out.println(SDF.format(new Date()) + ":Tell LocalFS set" + localSubindexPathStr + " to be deleted on Exit.");  //debug

                context.write(key, new Text(localSubindexPathStr));
            }

            System.out.println(SDF.format(new Date()) + ":" + updatedIndexCount + " index/images are updated to the main Lucene Index.");  //debug
            System.out.println(SDF.format(new Date()) + ": Consolidation Finished");  //debug
            indexWriter.forceMerge(3, true);
            indexWriter.commit();
            indexWriter.close();
            System.out.println(SDF.format(new Date()) + ": Lucene writer closed");  //debug

//            String localOutputPathStr = conf.get("indexer.localOutputPath", null);
//            System.out.println("localOutputPathStr = " + localOutputPathStr);  //debug
//            if (localOutputPathStr != null) {
//                System.out.println("localOutputPathStr found! copy to here");  //debug
//                hdfs.moveToLocalFile(new Path(outputPath), new Path(localOutputPathStr));
//                System.out.println("Copy finish");  //debug
//            }
            //hdfs.delete(new Path(outputPath), true);
            removeDir(outputPath, hdfs);
            hdfs.copyFromLocalFile(localFs.makeQualified(new Path(localMergedIndexPathStr)), hdfs.makeQualified(new Path(outputPath)));
            removeDir(localMergedIndexPathStr, localFs);    // Remove only after a successful call
            System.out.println(SDF.format(new Date()) + ":Reduce finished!");  //debug
        }

    }

    public int run(String[] args) throws Exception {
        System.out.println(SDF.format(new Date()) + ":Start run()");  //debug
//        if (args.length < 3) {
//            System.out.println("Usage: Indexer <input HIB> <output directory> <number of indexing nodes>");
//            System.exit(0);
//        }

//    Configuration conf = new Configuration();
        Configuration conf = super.getConf();
        FileSystem hdfs = FileSystem.get(conf);
        FileSystem localfs = FileSystem.getLocal(conf);

        if (conf.get("indexer.inputPath") == null || conf.get("indexer.outputPath") == null) {
            System.out.println("Please define indexer.inputPath & indexer.inputPath");
            System.exit(0);
        }
        //Attaching constant values to Configuration
        String inputPath = conf.get("indexer.inputPath");
        int defaultNodes = 5;
        conf.setInt("indexer.nodes", conf.getInt("indexer.nodes", defaultNodes));
//        if (args.length > 3) {
//            if (!localfs.getFileStatus((new Path(args[3])).getParent()).isDirectory()) {
//                System.out.println("Parent directory of <optional local output directory> " + args[3] + " does not exist.");
//                System.exit(0);
//            }
//            conf.set("indexer.localOutputPath", args[3]);
//        }

        conf.set("indexer.localTempPath", conf.get("indexer.localTempPath", "~/tmp"));

        Job job = Job.getInstance(conf, conf.get("mapreduce.job.name", "Indexer"));  //If param is null, use "Indexer"

        job.setJarByClass(Indexer.class);
        job.setMapperClass(IndexerMapper.class);
        job.setReducerClass(IndexerReducer.class);

//        job.setInputFormatClass(ImageBundleInputFormat.class);
        job.setInputFormatClass(IndexerInputFormat.class);
        job.setOutputKeyClass(BooleanWritable.class);
//        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(Text.class);

        job.setMaxMapAttempts(1);
        
        job.setNumReduceTasks(1);
        job.setMaxReduceAttempts(1);

        String outputPath_mapred = conf.get("indexer.outputPath") + "_mapred";
        removeDir(outputPath_mapred, hdfs);

        FileStatus[] listStatus = hdfs.listStatus(new Path(inputPath));
        StringBuilder pathListBuilder = new StringBuilder();
        //Pattern pureNumberPattern = Pattern.compile("^.+\\.dist\\.hib$");
        for (FileStatus status : listStatus) {
//            if (status.isFile() && pureNumberPattern.matcher(status.getPath().getName()).matches()){
            if (status.isFile()
                    && status.getPath().getName().endsWith(".dist.hib")
                    && status.getLen() > 0) {
                IndexerInputFormat.addInputPath(job, status.getPath());
                pathListBuilder.append(status.getPath().toString() + "\n");
            }
        }
        if (IndexerInputFormat.getInputPaths(job).length == 0) {
            System.out.println("No files are matched with filter. Target Distributed HIB files must have extension .dist.hib");
            System.exit(0);
        }

        Path hibPathList = new Path(inputPath.concat("hibFileList.txt"));
        System.out.println(SDF.format(new Date()) + ":Try to write the fileList to " + hibPathList.toString());
        //try{
        BufferedWriter br = new BufferedWriter(new OutputStreamWriter(hdfs.create(hibPathList, true)));
        System.out.println(SDF.format(new Date()) + ":BufferedWriter initialized");
        br.write(pathListBuilder.toString());
        br.close();
        System.out.println(SDF.format(new Date()) + ":Write finish!");
//        }catch(Exception e){
//            System.out.println("File not found");
//        }

        IndexerInputFormat.setInputPaths(job, hibPathList);
//        FileInputFormat.setInputDirRecursive(job, false);
//        FileInputFormat.setInputPathFilter(job, DistHibFilter.class);
//        FileInputFormat.addInputPath(job, new Path(inputPath));
        //FileInputFormat.setInputPaths(job, new Path(inputPath));
        FileOutputFormat.setOutputPath(job, new Path(outputPath_mapred));

        job.setNumReduceTasks(1);

        System.out.println(SDF.format(new Date()) + ":End of run()");  //debug
        return job.waitForCompletion(true) ? 0 : 1;

    }

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Indexer(), args);
        System.exit(res);
    }

}
