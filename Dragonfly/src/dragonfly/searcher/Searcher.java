/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dragonfly.searcher;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.semanticmetadata.lire.DocumentBuilder;
import net.semanticmetadata.lire.ImageSearchHits;
import net.semanticmetadata.lire.ImageSearcher;

import net.semanticmetadata.lire.imageanalysis.CEDD;            //ColorEdge
import net.semanticmetadata.lire.imageanalysis.FCTH;            //Fuzzy Color and Texture
import net.semanticmetadata.lire.imageanalysis.PHOG;            //Pattern Search
import net.semanticmetadata.lire.imageanalysis.ColorLayout;     //Color Search

import net.semanticmetadata.lire.impl.GenericFastImageSearcher;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.store.FSDirectory;

//import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.imaging.Imaging;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;

/**
 *
 * @author Joe
 */
public class Searcher {

    final static SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String[] _args = null;
    private Options _options = new Options();
    private CommandLine _cmd = null;

    public static void main(String[] args) throws IOException, Exception {
        Options options = new Options();

        options.addOption("numImage", true, "Maximum number of photo search result");
        options.addOption("alg", true, "Algorithm used on searching");
        options.addOption("localIndexDir", true, "Lucene Index in Local file system");
        options.addOption("inputImage", true, "Target image to be compared");
        options.addOption("outputResult", true, "Destination of result output");

        // create the parser
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try {
            // parse the command line arguments
            cmd = parser.parse(options, args);
        } catch (ParseException exp) {
            // oops, something went wrong
            System.err.println("Parsing failed.  Reason: " + exp.getMessage());
        }

        Searcher instance = new Searcher(cmd, args, options);
        int res = instance.run();
        System.exit(res);
    }

    public Searcher(CommandLine cmd, String[] args, Options options) {
        _cmd = cmd;
        _args = args;
        _options = options;
    }

    public int run() throws Exception {

//        if (conf.get("searcher.indexDir") == null) {
//            System.out.println("Please define searcher.indexDir");
//            System.exit(0);
//        }
        int similarityTheshold = 30;
        String searchAlg = _cmd.getOptionValue("alg", "CEDD");
        String indexDirLocalPathStr = _cmd.getOptionValue("localIndexDir", "~/Dragonfly/MyIndex");
        String indexDirHdfsPathStr = _cmd.getOptionValue("hdfsIndexDir", null);
        String inputImgPathStr = _cmd.getOptionValue("inputImage", null);
        String outputResultPathStr = _cmd.getOptionValue("outputResult", null);

        if (!(inputImgPathStr.isEmpty() || outputResultPathStr.isEmpty())
                || !indexDirHdfsPathStr.isEmpty()) {
            // Okay
        } else {
            System.out.println(SDF.format(new Date()) + ": Please define (inputImage + outputResult) for searching mode, OR hdfsIndexDir for preparation mode, or BOTH.");
            System.exit(0);
        }

        if (inputImgPathStr.isEmpty() || outputResultPathStr.isEmpty()) {
            System.out.println("Leaving: You are now executing in preparation mode only. Index has been fetched to local " + indexDirLocalPathStr + " as preparation.");
            System.exit(0);
        }

        File InputImgFile = new File(inputImgPathStr);
        if (!InputImgFile.isFile()) {
            System.out.println("You are giving an invalid input image path: " + inputImgPathStr);
            System.exit(0);
        }

        File outputResultFile = new File(outputResultPathStr);
        File outputResultFileBak = new File(outputResultPathStr + ".bak");

        if (outputResultFile.exists() && outputResultFile.isFile()) {
            if (outputResultFileBak.exists()) {
                outputResultFileBak.delete();
            }
            outputResultFile.renameTo(outputResultFileBak);
        }

        //System.out.println(SDF.format(new Date()) + ": Retriving BufferedImage");
        BufferedImage bufferedImage = Imaging.getBufferedImage(new File(inputImgPathStr));
        int x = Math.round(bufferedImage.getWidth() * 0.4f);
        int y = Math.round(bufferedImage.getHeight() * 0.4f);
        int w = Math.round(bufferedImage.getWidth() * 0.2f);
        int h = Math.round(bufferedImage.getHeight() * 0.2f);
        //System.out.println(SDF.format(new Date()) + ": Crop to target area");
        BufferedImage subimage = bufferedImage.getSubimage(x, y, w, h);

        IndexReader ir = DirectoryReader.open(FSDirectory.open(new File(indexDirLocalPathStr)));
        //searchAlg
        ImageSearcher searcher = null;
        switch (searchAlg) {
            case "CEDD":
                searcher = new GenericFastImageSearcher(similarityTheshold, CEDD.class);
                break;
            case "FC":
                searcher = new GenericFastImageSearcher(similarityTheshold, FCTH.class);
                break;
            case "PH":
                searcher = new GenericFastImageSearcher(similarityTheshold, PHOG.class);
                break;
            case "CL":
                searcher = new GenericFastImageSearcher(similarityTheshold, ColorLayout.class);
                break;
            default:
                searcher = new GenericFastImageSearcher(similarityTheshold, CEDD.class);
                break;
        }
        //ImageSearcher searcher = new GenericFastImageSearcher(30, AutoColorCorrelogram.class);

        // searching with a image file ...
        //System.out.println(SDF.format(new Date()) + ": Perform Search...");
        ImageSearchHits hits = searcher.search(subimage, ir);
        // searching with a Lucene document instance ...
//        ImageSearchHits hits = searcher.search(ir.document(0), ir);

        //System.out.println(SDF.format(new Date()) + ": Perform done with " + hits.length() + " images found! Printing the result...");
        PrintWriter resultWriter = new PrintWriter(outputResultPathStr);
        resultWriter.write("");
        for (int i = 0; i < hits.length(); i++) {
            Document currDoc = hits.doc(i);
            String fileName = currDoc.getValues(DocumentBuilder.FIELD_NAME_IDENTIFIER)[0];
            IndexableField field = currDoc.getField("site");

            URI uri = new URI(fileName);
            String domain = uri.getHost();
            String deducedSite = "http://" + (!domain.startsWith("www.") ? domain.substring(domain.indexOf(".") + 1) : domain);

            String site = (field == null || field.stringValue().isEmpty()) ? deducedSite : field.stringValue();
            if (site.isEmpty()) {
                site = "http://www.zalora.com.hk";
            }
            resultWriter.write(fileName + "|" + site + "\n");
            System.out.println(fileName + "|" + site);
            //System.out.println(hits.score(i) + ": \t" + fileName);
            if (i > 60) {
                break;
            }
        }
        resultWriter.close();

        //System.out.println(SDF.format(new Date()) + ": Congraduation! " + hits.length() + " images are found! Check " + outputResultPathStr);
        return 0;
    }

}
